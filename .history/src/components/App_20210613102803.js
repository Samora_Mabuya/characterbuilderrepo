import logo from './logo.svg';
import './App.css';
import '../Sass/App.scss'
import characterbuilderform from './characterbuilderform/characterbuilderform'
import { Browser as Router, Route, Switch } from 'react-router-dom'

function App() {
    return ( <
        div className = "App" >
        <
        Router >
        <
        Switch >
        <
        Route exact path = '/'
        component = { characterbuilderform }
        /> <
        /Switch> <
        /Router>

        <
        /div>
    );
}

export default App;