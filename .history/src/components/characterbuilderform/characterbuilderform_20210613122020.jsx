import React from 'react'
import './characterbuilderform.scss'

const characterbuilderform = () => {


  const Steps = (props) => {
    return <div className="Steps">{props.steps}</div>

  }
  return (
    <div>

      <div className="headings">
      <h1> Character builder</h1>
      <h2> Please complete the steps below</h2>
      </div>

      <Steps steps="Step 1"/>


      <div className="form">

        <div className="Form">
          
          <label>Name</label>
          <input type="text" className="formInput"/>

          <label>Surname</label>
          <input type="text" className="formInput"/>
        </div>

        <button className="nextbtn">
          next
        </button>
        
      </div>


    </div>
  )
}

export default characterbuilderform;