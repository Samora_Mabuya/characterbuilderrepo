import React, { useState, useRef } from "react";

import "./characterbuilderform.scss";
import { Formik, Form, Field } from "formik";
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import Moment from "react-moment";
import "moment-timezone";
import { BlockPicker } from "react-color";
import * as Yup from "yup";
// heads
import femalehead from "../../images/f-head.png";
import malehead from "../../images/m-head.png";
import oldmalehead from "../../images/old-m-head.png";
import oldfemalehead from "../../images/old-f-head.png";
// upperbody
import chef from "../../images/chef.png";
import yoga from "../../images/yoga.png";
import developer from "../../images/developer.png";
import Influencer from "../../images/social-media.png";
// pants
import { ReactComponent as Pants} from "../../images/pants.svg";

import { values } from "lodash";
// import femalehead from "../../images/f-head.png";
// import femalehead from "../../images/f-head.png";
// import femalehead from "../../images/f-head.png";
// import femalehead from "../../images/f-head.png";

const Characterbuilderform = () => {
  const [entry, setentry] = useState({
    firstname: "",
    surname: "",
    sex: "",
    occupation: "",
  });

  const initalValues = {
    firstname: "",
    surname: "",
    sex: "",
  };

  const validationSchema = Yup.object({
    firstname: Yup.string().required("Required"),
    sex: Yup.string().required("Required"),
  });

  const Steps = (props) => {
    return <div className="Steps">{props.steps}</div>;
  };

  const [currentPage, setcurrentPage] = useState(0);

  const handleNext = (newEntry) => {
    setentry((prev) => ({ ...prev, ...newEntry }));

    setcurrentPage((prev) => prev + 1);
  };

  const handlePrev = (newEntry) => {
    setentry((prev) => ({ ...prev, ...newEntry }));
    setcurrentPage((prev) => prev - 1);
  };

  const pages = [
    <PageOne next={handleNext} data={entry} />,
    <PageTwo next={handleNext} prev={handlePrev} data={entry} />,
    <PageThree next={handleNext} prev={handlePrev} data={entry} />,
  ];

  return (
    <div className="Characterbuilderform">
      <div className="headings">
        <h1> Character builder</h1>
        <h2> Please complete the steps below</h2>
      </div>

      <div className="bodyParts">
        <div className="headWrapper">
          <img className="head" src={femalehead} alt="femalehead" />
{/* 
          {entry.sex === "Male" && currentPage === 2 ? (
            <img className="head" src={malehead} alt="malehead" />
          ) : null}

          {entry.sex === "Female" && currentPage === 2 ? (
            <img className="head" src={femalehead} alt="femalehead" />
          ) : null} */}
        </div>

        <div className="bodyWrapper">
          <img className="upperbody" src={yoga} alt="Yoga" />
{/* 
          {entry.occupation === "Chef" && currentPage === 2 ? (
            <img className="upperbody" src={chef} alt="Chef" />
          ) : null}

          {entry.occupation === "Yoga Instructor" && currentPage === 2 ? (
            <img className="upperbody" src={yoga} alt="Yoga" />
          ) : null}

          {entry.occupation === "Developer" && currentPage === 2 ? (
            <img src={developer} alt="Developer" />
          ) : null}

          {entry.occupation === "Social media influencer" &&
          currentPage === 2 ? (
            <img src={Influencer} alt="Influencer" />
          ) : null} */}
        </div>

        <div className="pantsWrapper" fill="blue" style={{ color: "blue"}}>
        <Pants className="pants" fill="blue" src={Pants} alt="pants" style={{ color: "blue" }} ><path fill="black"></path></Pants>
          {/* {currentPage === 2 ? (
            <img className="pants" src={pants} alt="pants" />
          ) : null} */}
        </div>

        <div className="contextWrapper">
          <h2 className="name">I am {entry.firstname}</h2>

          {/* {currentPage === 2 ? (
            <h2 className="name">I am {entry.firstname}</h2>
          ) : null} */}
        </div>

        {pages[currentPage]}
        {/* <Steps steps="Step 1" /> */}
      </div>
    </div>
  );
};

const PageOne = (props) => {
  const GenderOption = [
    { label: "Option 1", value: "Male" },
    { label: "Option 2", value: "Female" },
  ];
  const handleSubmit = (values) => {
    props.next(values);
  };
  return (
    <Formik
      initialValues={props.data}
      // initialValues={initialValues}
      onSubmit={handleSubmit}
      // validationSchema={validationSchema}
    >
      {() => (
        <Form>
          <div className="form">
            <div className="Form">
              <label>Name</label>
              <Field name="firstname" className="formInput" />
              <label>Surname</label>
              <Field name="surname" className="formInput" />
            </div>
            <div className="radioGroup">
              <h2>Select Gender</h2>
              <ul className="radioOptions">
                <li>
                  <label>Male</label>
                  <Field
                    type="radio"
                    name="sex"
                    value="Male"
                    options={GenderOption}
                  />
                </li>
                <li>
                  <label>Female</label>
                  <Field
                    type="radio"
                    name="sex"
                    value="Female"
                    options={GenderOption}
                  />
                </li>
              </ul>
            </div>

            <div className="buttondiv">
              <button type="submit" className="nextbtn">
                Next
              </button>
            </div>
          </div>
        </Form>
      )}
    </Formik>
  );
};

const PageTwo = (props) => {
  const [selectedDate, setSelectedDate] = useState(new Date());
  const [Color, setColor] = useState("#2ccce4");

  const JobSelect = [
    { label: "Option 1", value: "Chef" },
    { label: "Option 2", value: "Yoga Instructor" },
    { label: "Option 3", value: "Developer" },
    { label: "Option 4", value: "Social media influencer" },
  ];

  const handleSubmit = (values) => {
    props.next(values, true);
  };
  return (
    <Formik initialValues={props.data} onSubmit={handleSubmit}>
      {({ values }) => (
        <Form>
          <div className="secondform">
            <div className="Datepicker">
              <DatePicker
                className="datepicker"
                selected={selectedDate}
                onChange={(date) => setSelectedDate(date)}
                dateFormat="dd/MMMM/yyyy"
              />
            </div>

            <div className="Occupation">
              <Field className="selectRole" name="occupation" as="select">
                <option name="occupation" value="Chef" options={JobSelect}>
                  Chef
                </option>
                <option
                  name="occupation"
                  value="Yoga Instructor"
                  options={JobSelect}
                >
                  Yoga Instructor
                </option>
                <option name="occupation" value="Developer" options={JobSelect}>
                  Developer
                </option>
                <option
                  name="occupation"
                  value="Social media influencer"
                  options={JobSelect}
                >
                  Social media influencer
                </option>
              </Field>
            </div>

            <div className="colorPicker">
              <span className="color" style={{ color: Color }}>
                Select Color
              </span>

              <div className="BlockPickerWrapper">
                <BlockPicker
                  className="BlockPicker"
                  color={Color}
                  onChangeComplete={(Color) => setColor(Color.hex)}
                />
              </div>
            </div>

            <div className="buttondiv">
              <button type="submit" className="nextbtn">
                Finish
              </button>

              <button
                type="button"
                className="prevbtn"
                onClick={() => props.prev(values)}
              >
                Back
              </button>
            </div>
          </div>
        </Form>
      )}
    </Formik>
  );
};
const PageThree = (props) => {
  return (
    <Formik initialValues={props.data}>
      {({ values }) => (
        <Form>
          <div className="buttondiv">
            <button
              type="button"
              className="prevbtntwo"
              onClick={() => props.prev(values)}
            >
              Back
            </button>
          </div>
        </Form>
      )}
    </Formik>
  );
};

export default Characterbuilderform;
