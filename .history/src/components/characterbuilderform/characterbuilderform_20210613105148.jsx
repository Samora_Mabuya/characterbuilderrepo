import React from 'react'
import './characterbuilderform.scss'

const characterbuilderform = () => {


  const Steps = (props) => {
    return <span className="Steps">{props.steps}</span>

  }
  return (
    <div>

      <div className="headings">
      <h1> Character builder</h1>
      <h2> Please complete the steps below</h2>
      </div>

      <div>
        <Steps steps="Step 1"/>
        
      </div>


    </div>
  )
}

export default characterbuilderform;