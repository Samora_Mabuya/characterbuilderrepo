import React, { useState } from 'react'

import "./characterbuilderform.scss";
import { Formik, Form, Field } from "formik";

const Characterbuilderform = () => {

  const [entry, setentry] = useState({
    firstname: "",
    surname: ""
  });

  const [currentPage, setcurrentPage] = useState(0);
  const pages = [<PageOne/>, <PageTwo/>]
  
  const Steps = (props) => {
    return <div className="Steps">{props.steps}</div>;
  };

    const ButtonText = (props) => {
      return <button className="buttonText">{props.btnText}</button>;
  };

  return (
    <div>
      <div className="headings">
        <h1> Character builder</h1>
        <h2> Please complete the steps below</h2>
      </div>

      <Steps steps="Step 1" />
      {/* {pages[entry]} */}
    </div>
  );
};
  const PageOne = () => {
    <Formik>
      {() => (
        <Form>
          <div className="form">
            <div className="Form">
              <label>Name</label>
              <input type="text" className="formInput" />
              <label>Surname</label>
              <input type="text" className="formInput" />
            </div>
            <div className="buttondiv">
            <ButtonText btnText="nextbtn" type="submit"/>
            </div>
          </div>
        </Form>
      )}
    </Formik>;
  };

  const PageTwo = () => {
    <Formik>
      {() => (
        <Form>
          <div className="form">
            <div className="Form">
              <label>Name</label>
              <input type="text" className="formInput" name="firstname" />
              <label>Surname</label>
              <input type="text" className="formInput" name="surname" />
            </div>
            <div className="buttondiv">
            <ButtonText className="next" btnText="nextbtn" type="submit"/>
            <ButtonText className="prev" btnText="prevbtn" type="submit"/>
            </div>
          </div>
        </Form>
      )}
    </Formik>;
  };


export default Characterbuilderform;
