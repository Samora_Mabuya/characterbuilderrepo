import React, { useState } from "react";

import "./characterbuilderform.scss";
import { Formik, Form, Field } from "formik";

const Characterbuilderform = () => {
  const Steps = (props) => {
    return <div className="Steps">{props.steps}</div>;
  };

  const [entry, setentry] = useState({
    firstname: "",
    surname: "",
  });

  const [currentPage, setcurrentPage] = useState(0);

  const handleNext = (newEntry) => {
    setentry(prev => ({...prev, ...newEntry}))
    setcurrentPage(prev => prev + 1)
    
  }

  const handlePrev = (newEntry) => {
    setentry(prev => ({...prev, ...newEntry}))
    setcurrentPage(prev => prev - 1)
    
  }


  const pages = [
  <PageOne next={handleNext} data={entry} />]
  // <PageTwo next={handleNext} prev={handlePrev} data={entry} />]

  return (
    <div>
      <div className="headings">
        <h1> Character builder</h1>
        <h2> Please complete the steps below</h2>
      </div>

      <Steps steps="Step 1" />
      
      {pages[currentPage]}
    </div>
  );
}
const PageOne = (props) => {
  const Submit = (values) => {
    props.next(values)
  }
  return (
    <Formik
    
    initialValues={props.entry}
    onSubmit={Submit}
    >

      {() => (
        <Form>
          <div className="form">
            <div className="Form">
              <label>Name</label>
              <Field type="text" className="formInput" />
              <label>Surname</label>
              <Field type="text" className="formInput" />
            </div>
            <div className="buttondiv">
              <button type="submit" className="nextbtn">Next</button>
            </div>
          </div>
        </Form>
      )}
    </Formik>
  );
};

const PageTwo = () => {
  return (
    <Formik>
      {() => (
        <Form>
          <div className="form">
            <div className="Form">
              <label>Name</label>
              <Field type="text" className="formInput" name="firstname" />
              <label>Surname</label>
              <Field type="text" className="formInput" name="surname" />
            </div>
            <div className="buttondiv">
            <button type="submit" className="nextbtn">Submit</button>
            <button type="submit" className="prevbtn">Previous</button>
            </div>
          </div>
        </Form>
      )}
    </Formik>
  );
};

export default Characterbuilderform;
