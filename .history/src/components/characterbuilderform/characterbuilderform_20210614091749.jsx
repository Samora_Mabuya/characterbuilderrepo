import React, { useState } from "react";

import "./characterbuilderform.scss";
import { Formik, Form, Field } from "formik";
import DatePicker from "react-datepicker";
import 'react-datepicker/dist/react-datepicker.css'
import Moment from 'react-moment';
import 'moment-timezone';
const Characterbuilderform = () => {
  const [entry, setentry] = useState({
    firstname: "",
    surname: "",
    selected: "",
  });


  const Steps = (props) => {
    return <div className="Steps">{props.steps}</div>;
  };

  const [currentPage, setcurrentPage] = useState(0);
  

  const submitForm = (formData) => {
    console.log("Form submitted", formData);
  };

  const handleNext = (newEntry, final = false) => {
    setentry((prev) => ({ ...prev, ...newEntry }));

    if (final) {
      submitForm(newEntry);
      return;
    }
    setcurrentPage((prev) => prev + 1);
  };

  const handlePrev = (newEntry) => {
    setentry((prev) => ({ ...prev, ...newEntry }));
    setcurrentPage((prev) => prev - 1);
  };

  const pages = [
    <PageOne next={handleNext} data={entry} />,
    <PageTwo next={handleNext} prev={handlePrev} data={entry} />,
  ];



  return (
    <div>
      <div className="headings">
        <h1> Character builder</h1>
        <h2> Please complete the steps below</h2>
      </div>

      {/* <Steps steps="Step 1" /> */}

      {pages[currentPage]}
    </div>
  );
};
const PageOne = (props) => {
  const handleSubmit = (values) => {
    props.next(values);
  };
  return (
    <Formik 
    initialValues={props.data} onSubmit={handleSubmit}>
      {() => (
        <Form>
          <div className="form">
            <div className="Form">
              <label>Name</label>
              <Field name="firstname" className="formInput" />
              <label>Surname</label>
              <Field name="surname" className="formInput" />
            </div>
            <div className="radioGroup">

            <h2>Select Gender</h2>
            <ul className="radioOptions">
            <li>
              <label>Male</label>
              <Field type="radio" name="selected" value="male" />
            </li>
            <li>
              <label>Female</label>
              <Field type="radio" name="selected" value="female" />
            </li>

            </ul>

          
            </div>
            
            <div className="buttondiv">
              <button type="submit" className="nextbtn">
                Next
              </button>
            </div>
          </div>
        </Form>
      )}
    </Formik>
  );
};

const PageTwo = (props) => {

  // const [startDate, setStartDate] = useState(new Date());
  const [selectedDate, setSelectedDate] = useState(new Date());

  return <Formik>{() => (
    <Form>


        <DatePicker selected={selectedDate}
        onChange={(date => setSelectedDate (date))}
        dateFormat='dd/MMMM/yyyy'/>
    
    </Form>
  )}
  
    </Formik>;
};

export default Characterbuilderform;
