import logo from "./logo.svg";
import "./App.css";
import characterbuilderform from "./characterbuilderform/characterbuilderform";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";

function App() {
  return (
    <div className="App">
      <Router>
        <Switch>
          <Route exact path="/" component={characterbuilderform} />
        </Switch>
      </Router>
    </div>
  );
}

export default App;
